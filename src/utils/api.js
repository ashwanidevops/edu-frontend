import Axios from 'axios';

export const signinRequest = async (email, password) => {
    const body = {
        email : email,
        password: password
    };
    try {
        const data =  await Axios.post(`${process.env.REACT_APP_BASE_URL}/user/login/`, body);
        return data;
    } catch (error) {
        console.log("API ERROR: "+error);
        return error;
    }
}

export const signupRequest = async (data) => {
    const body = {
        email : data[0],
        password: data[1],
        type: data[2],
        name: data[3],
        school: data[4],
        std: data[5]
    };
    try {
        const data =  await Axios.post(`${process.env.REACT_APP_BASE_URL}/user/signup/`, body);
        return data;
    } catch (error) {
        console.log("API ERROR: "+error);
        return error;
    }
}

export const fetchProfile = async (token) => {
    try {
        const data = await Axios.get(
            `${process.env.REACT_APP_BASE_URL}/profile`, 
            { headers: {"Authorization" : `Bearer ${token}`}});
            return data;
    } catch (error) {
        console.log("API ERROR: "+error);
        return error;
    }
}

export const fetchUserdata = async (token) => {
    try {
        const data = await Axios.get(
            `${process.env.REACT_APP_BASE_URL}/userdata`, 
            { headers: {"Authorization" : `Bearer ${token}`}});
            return data;
    } catch (error) {
        console.log("API ERROR: "+error);
        return error;
    }
}

export const fetchTestData = async () => {
    try {
        const data = await Axios.get(process.env.REACT_APP_BASE_URL);
        return data;
    } catch (error) {
        console.log("API ERROR: "+error);
        return error;
    }
}

export const fetchFreeTestData = async (std, subject) => {
    const body = {
        std: std, 
        subject: subject
    };
    try {
        const data = await Axios.post(`${process.env.REACT_APP_BASE_URL}/freetest`, body);
        return data;
    } catch (error) {
        console.log("API ERROR: "+error);
        return error;
    }
}

export const fetchSchools = async () => {
    try {
        const data = await Axios.get(`${process.env.REACT_APP_BASE_URL}/school`);
        return data;
    } catch (error) {
        console.log("API ERROR: "+error);
        return error;
    }
}

export const fetchClasses = async () => {
    try {
        const data = await Axios.get(`${process.env.REACT_APP_BASE_URL}/standard`);
        return data;
    } catch (error) {
        console.log("API ERROR: "+error);
        return error;
    }
}

export const fetchSubjects = async () => {
    try {
        const data = await Axios.get(`${process.env.REACT_APP_BASE_URL}/subject`);
        return data;
    } catch (error) {
        console.log("API ERROR: "+error);
        return error;
    }
}