import React from "react";

import classes from './TestSeriesElement.module.css';
import icon from '../../../assets/images/brandLogo.jpg';
import SubmitButton from '../../Button/SubmitButton';

const TestSeriesElement = props => {

    return (
        <div className={classes.container}>
            <div className={classes.iconWrap}>
                <img className={classes.icon} src={icon} alt="icon" />
            </div>
            <div className={classes.textContainer}>
                <p> Science Test Series</p>
                <p> 5/10 Tests</p>
            </div>
            <div className={classes.buttonWrap}>
                <SubmitButton 
                    title="Go to Test Series" 
                    color='series'
                />
            </div>
        </div>
    );
}

export default TestSeriesElement;