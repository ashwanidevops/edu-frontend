import React from "react";


import classes from "./Dashboard.module.css";
import TestSeriesElement from "./TestSeriesElement/TestSeriesElement";

const Dashboard = props => {

    return (
        <div className={classes.main_container}>
            <h3 className={classes.heading}>Your Recent Test Series</h3>
            <div className={classes.container}>
                <TestSeriesElement />
                <TestSeriesElement />
                <TestSeriesElement />
            </div>
        </div>
        
    );
}

export default Dashboard;