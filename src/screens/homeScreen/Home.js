import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from 'react-redux';

import Header, { DashHeader } from "../../component/Header/Header";
import Dashboard from "../../component/Dashboard/Dashboard";
import FreeTest from "../../component/FreeTest/FreeTest";
import Footer from "../../component/Footer/Footer";
import './Home.css';
import { getFreeTest, getTest } from "../../store/actions/freeTestActions";
import { getSchools, getClasses, getSubjects } from "../../store/actions/homeUnauthActions";
import { getUserdata, getProfile } from "../../store/actions/profileActions";

// const classes = ['6th', '7th', '8th', '9th', '10th'];
// const schools = ['Kendriya Vidyalaya', 'Army Public School', 'D.A.V.', 'Govt. Model School'];
// const subjects = ['English', 'Maths', 'Physics', 'Chemistry', 'Biology'];

const Home = (props) => {
    const [testArray, setTestArray] = useState([]);
    const [freeTestArray, setFreeTestArray] = useState({});
    const [schoolArray, setSchoolArray] = useState([]);
    const [stdArray, setStdArray] = useState([]);
    const [subjectArray, setSubjectArray] = useState([]);
    const [profile, setProfile] = useState({});
    const [token, setToken] = useState(localStorage.getItem('TOKEN'));

    const testData = useSelector((state) => state.fetchTest);
    const freeTestData = useSelector((state) => state.fetchFreeTest);
    const schoolData = useSelector((state) => state.schoolsName);
    const stdData = useSelector((state) => state.classesName);
    const subjectData = useSelector((state) => state.subjectsName);
    const {loading} = useSelector((state) => state.fetchFreeTest);
    const profileData = useSelector((state) => state.getProfile);
    const userData = useSelector((state) => state.getUserdata);

    const dispatch = useDispatch();
    let navigate = useNavigate();

    useEffect(() => {
        // const token = localStorage.getItem('TOKEN');
        dispatch(getSchools());
        dispatch(getSubjects());
        dispatch(getClasses());
        dispatch(getTest());
        if(token){
            dispatch(getProfile(token));
            dispatch(getUserdata(token));
        }
    }, []);

    useEffect(() => {
        setTestArray(testData.data.tests);
        if(testData.loading === false){
            // console.log(testArray);
        }
        setFreeTestArray(freeTestData.data);
        if(freeTestData.loading === false){
            console.log(freeTestArray);
        }
        setSchoolArray(schoolData.data.schools);
        if(schoolData.loading === false) {
            // console.log(schoolArray);
        }
        setStdArray(stdData.data.stds);
        if(stdData.loading === false) {
            // console.log(stdArray);
        }
        setSubjectArray(subjectData.data.subjects);
        if(subjectData.loading === false) {
            // console.log(subjectArray);
        }
    });

    useEffect(() => {
        if(profileData.loading === false){
            setProfile(profileData.data)
            console.log(profileData.data);
        }
        if(userData.loading === false){
            console.log(userData);
        }
    },[profileData, userData]);

    const freeTestSubmitHandler = (name, std, subject) => {
        // console.log(`${name} ${std} ${subject}`);
        dispatch(getFreeTest( std, subject ));
        // setTimeout(() => {
        //     navigate(`/test/`);
        // }, 2000);
        navigate(`/test/`);
    }


    return (
        <div>
            {/* { token ?  */}
            {/* <div>
                <DashHeader
                    name={profile.name ? profile.name : null}
                />
                <Dashboard />
            </div> */}
            {/* :  */}
            <div>
                <Header 
                    classes={stdArray} 
                    subjects={subjectArray} 
                    schools={schoolArray}
                    name={profile.name ? profile.name : null}
                />
                <main>
                    <FreeTest 
                        classes={stdArray} 
                        subjects={subjectArray} 
                        makeAction={(name, std, subject) => freeTestSubmitHandler(name, std, subject)}
                    />
                </main>
            </div> 
            {/* }  */}
            {/* <h3>Home screen</h3>
            <ul>
                {testArray.map(test => {
                    return (
                        <li key={test.testId}>
                            <Link to={'/test/'+test.testId}>{test.title}</Link>
                        </li>
                    );
                })}
            </ul> */}
            <footer className="Homefooter">
                <Footer />
            </footer>
        </div>
    );
}

export default Home;