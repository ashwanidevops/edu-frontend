import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import { Provider } from 'react-redux';
import Test from './screens/testScreen/Test';
import Home from './screens/homeScreen/Home';
import Signup from './screens/signup/Signup';
import Signin from './screens/signin/Signin';
import './App.css';


function App() {
  return (
    // <Provider store={store}>
      <Router>
      <div>
        {/* <div className="App">
          <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/test">Test</Link>
          </li>
          </ul>
        </div> */}
        <Routes>
          <Route exact path='/' element={< Home />}></Route>
          <Route exact path='/test/' element={< Test />}></Route>
          <Route exact path='/signin/' element={< Signin />}></Route>
          <Route exact path='/signup/' element={< Signup />}></Route>
        </Routes>
      </div>
    </Router>
    // </Provider> 
    
    
  );
}

export default App;
