import { 
    GET_PROFILE_FAIL, 
    GET_PROFILE_REQUEST, 
    GET_PROFILE_SUCCESS, 
    GET_USERDATA_FAIL, 
    GET_USERDATA_REQUEST,
    GET_USERDATA_SUCCESS 
} from "../constants/profileConstants";

function getProfileReducer(state = { data: {} }, action) {
    switch (action.type) {
        case GET_PROFILE_REQUEST:
            return { loading: true, data: {} };
        case GET_PROFILE_SUCCESS:
            // console.log(action.payload);
            return { 
                loading: false, 
                data: {
                    name: action.payload.name,
                    email: action.payload.email,
                    school: action.payload.school,
                    std: action.payload.class
                } 
            };
        case GET_PROFILE_FAIL:
            return { loading: false, error: action.payload };
        default:
            return state;
    }
}

function getUserdataReducer(state = { data: {} }, action) {
    switch (action.type) {
        case GET_USERDATA_REQUEST:
            return { loading: true, data: {} };
        case GET_USERDATA_SUCCESS:
            // console.log(action.payload);
            return { 
                loading: false, 
                data: {
                    tests: action.payload.tests
                } 
            };
        case GET_USERDATA_FAIL:
            return { loading: false, error: action.payload };
        default:
            return state;
    }
}

export { getProfileReducer, getUserdataReducer };